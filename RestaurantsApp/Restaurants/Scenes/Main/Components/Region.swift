//
//  Region.swift
//  FoursquareSDK
//
//  Created by Martin Pastorin on 21/12/2019.
//  Copyright © 2019 Martin Pastorin. All rights reserved.
//

import Foundation
import MapKit

typealias Region = MKCoordinateRegion

extension Region {

    init(from viewport: SDKViewport) {
        let span = MKCoordinateSpan(latitudeDelta: viewport.latitudeDelta,
                                    longitudeDelta: viewport.longitudeDelta)
        self.init(center: viewport.center, span: span)
    }
}
