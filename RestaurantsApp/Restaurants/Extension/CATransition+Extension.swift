//
//  CATransition+Extension.swift
//  DottRestaurants
//
//  Created by Martin Pastorin on 20/12/2019.
//  Copyright © 2019 Martin Pastorin. All rights reserved.
//

import UIKit

extension CATransition {
    static func fade(with duration: CFTimeInterval = 0.25) -> CATransition {
        let transition = CATransition()
        transition.duration = duration
        transition.timingFunction = CAMediaTimingFunction(name: .easeInEaseOut)
        transition.type = .fade
        transition.fillMode = .both
        return transition
    }
}

