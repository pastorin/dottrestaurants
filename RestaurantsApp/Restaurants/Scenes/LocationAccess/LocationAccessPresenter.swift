//
//  LocationAccessPresenter.swift
//  DottRestaurants
//
//  Created by Martin Pastorin on 21/12/2019.
//  Copyright (c) 2019 Martin Pastorin. All rights reserved.
//

import UIKit

protocol LocationAccessPresenterInterface: AnyObject {
    func presentAccessStatus(response: LocationAccessModels.FetchAccessStatus.Response)
}

class LocationAccessPresenter: LocationAccessPresenterInterface {

    // MARK: - Properties
    private weak var displayer: LocationAccessDisplayInterface?

    // MARK: - Init
    init(displayer: LocationAccessDisplayInterface) {
        self.displayer = displayer
    }

    // MARK: - LocationAccessPresenterInterface
    func presentAccessStatus(response: LocationAccessModels.FetchAccessStatus.Response) {
        displayer?.displayAccessStatus(viewModel: .init(response))
    }
}

// MARK: - Models
extension LocationAccessModels.FetchAccessStatus.ViewModel {
    init(_ response: LocationAccessModels.FetchAccessStatus.Response) {
        if response.accessDenied {
            title = "Location access disabled".uppercased()
            description = "Location access is required to display recommended restaurants around you.\n\nTo allow it you need to do it manually from the settings."
            buttonTitle = "Go to Settings".uppercased()
        } else {
            title = "Location access".uppercased()
            description = "Location access is required to display recommended restaurants around you"
            buttonTitle = "Enable location".uppercased()
        }
    }
}

