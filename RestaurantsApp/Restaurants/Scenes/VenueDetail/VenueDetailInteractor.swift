//
//  VenueDetailInteractor.swift
//  DottRestaurants
//
//  Created by Martin Pastorin on 22/12/2019.
//  Copyright (c) 2019 Martin Pastorin. All rights reserved.
//

protocol VenueDetailInteractorInterface {
    func fetchVenue(request: VenueDetailModels.FetchVenue.Request)
}

protocol VenueDetailDataStoreDelegate: AnyObject {
    func venueDetailDidUpdate(_ venue: SDKVenue)
}

protocol VenueDetailDataStore: AnyObject {
    var delegate: VenueDetailDataStoreDelegate? { get set }
}

struct VenueDetailData {
    var venue: SDKVenue
}

class VenueDetailInteractor: VenueDetailInteractorInterface {

    // MARK: - Properties
    private let worker: VenueDetailWorkerInterface
    private let presenter: VenueDetailPresenterInterface
    weak var delegate: VenueDetailDataStoreDelegate?
    var data: VenueDetailData

    // MARK: - Init
    init(data: VenueDetailData, worker: VenueDetailWorkerInterface, presenter: VenueDetailPresenterInterface) {
        self.data = data
        self.presenter = presenter
        self.worker = worker
        self.worker.delegate = self
    }

    convenience init(data: VenueDetailData, presenter: VenueDetailPresenterInterface) {
        let worker = VenueDetailWorker()
        self.init(data: data, worker: worker, presenter: presenter)
    }

    // MARK: - VenueDetailInteractorInterface
    func fetchVenue(request: VenueDetailModels.FetchVenue.Request) {
        worker.fetchVenueDetail(for: data.venue.id)
        presentVenue()
    }

    // MARK: - Helper
    func presentVenue() {
        presenter.presentVenue(response: .init(venue: data.venue))
    }
}

// MARK: - VenueDetailDataStore
extension VenueDetailInteractor: VenueDetailDataStore {

}

// MARK: - VenueDetailWorkerDelegate
extension VenueDetailInteractor: VenueDetailWorkerDelegate {
    func workerDidFinishFetchingVenueDetail(_ worker: VenueDetailWorker, with result: VenueDetailWorkerInterface.RequestVenueDetailResult) {
        guard case let .success(model) = result else {
            return
        }
        data.venue = model
        delegate?.venueDetailDidUpdate(model)
        presentVenue()
    }
}
