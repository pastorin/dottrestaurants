//
//  VenueInfoView.swift
//  DottRestaurants
//
//  Created by Martin Pastorin on 22/12/2019.
//  Copyright © 2019 Martin Pastorin. All rights reserved.
//

import UIKit

protocol VenueInfoViewModel: VenueSocialViewModel {
    var title: String { get }
    var subtitle: String { get }
    var description: String? { get }
}

class VenueInfoView: UIView {

    // MARK: - Properties
    private(set) weak var titleLabel: UILabel!
    private(set) weak var subtitleLabel: UILabel!
    private(set) weak var descriptionLabel: UILabel!
    private(set) weak var socialView: VenueSocialView!

    var viewModel: VenueInfoViewModel? {
        didSet {
            updateContent()
        }
    }

    // MARK: - Initializer
    convenience init() {
        self.init(frame: .zero)
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupUI()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupUI()
    }

    // MARK: - Setup
    private func setupUI() {
        let titleLabel = UILabel()
        titleLabel.setStyle(.bold(size: StyleKit.FontSizes.large,
                             color: StyleKit.Color.label))

        let socialView = VenueSocialView()
        socialView.snp.makeConstraints {
            $0.height.equalTo(20)
        }

        let subtitleLabel = UILabel()
        subtitleLabel.numberOfLines = 0
        subtitleLabel.setStyle(.regular(size: StyleKit.FontSizes.regular,
                                   color: StyleKit.Color.label1))

        let descriptionLabel = UILabel()
        descriptionLabel.numberOfLines = 0
        descriptionLabel.setStyle(.regular(size: StyleKit.FontSizes.regular,
                                      color: StyleKit.Color.label2))

        let titleStack = [titleLabel, .spacing, socialView]
            .stacked(.horizontal)

        let mainStack = [titleStack, subtitleLabel, descriptionLabel]
            .stacked(.vertical)
            .spacing(4, after: titleLabel)
            .spacing(8)
        addSubview(mainStack)
        mainStack.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }

        self.titleLabel = titleLabel
        self.subtitleLabel = subtitleLabel
        self.descriptionLabel = descriptionLabel
        self.socialView = socialView
        
    }

    // MARK: Update
    private func updateContent() {
        guard let viewModel = viewModel else {
            defaultContent()
            return
        }

        socialView.viewModel = viewModel

        titleLabel.text = viewModel.title
        subtitleLabel.text = viewModel.subtitle
        descriptionLabel.text = viewModel.description

        descriptionLabel.isHidden = viewModel.description.isEmptyOrWhitespace
    }

    private func defaultContent() {
        socialView.viewModel = nil
        titleLabel.text = nil
        subtitleLabel.text = nil
        descriptionLabel.text = nil
    }

}
