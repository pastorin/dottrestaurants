//
//  SessionManager.swift
//  RestaurantsSDK
//
//  Created by Martin Pastorin on 20/12/2019.
//  Copyright © 2019 Martin Pastorin. All rights reserved.
//

import Foundation
import ReactiveSwift
import Alamofire
import AlamofireActivityLogger

extension SessionManager {
    // MARK: -
    typealias Producer = SignalProducer<Data, Error>

    // MARK: - Public
    func request(from source: URLRequestConvertible) -> Producer {
        return producer {
            self.request(source)
        }
    }

    // MARK: - Helpers
    private func producer(with source: @escaping () -> DataRequest) -> Producer {
        precondition(!startRequestsImmediately, "Requests should be started by the producer.")

        return Producer { observer, lifetime in
            let request = source()
            request.validate()
                .log(level: .info, options: [])
                .log(level: .error)
                .responseData { response in
                    guard !lifetime.hasEnded else {
                        return
                    }

                    switch response.result {
                    case .success(let value):
                        observer.send(value: value)
                        observer.sendCompleted()
                    case .failure(let error):
                        observer.send(error: error)
                    }
                }

            request.resume()

            lifetime.observeEnded {
                request.cancel()
            }
        }
    }
}
