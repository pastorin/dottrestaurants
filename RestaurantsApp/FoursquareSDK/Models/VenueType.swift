//
//  VenueType.swift
//  RestaurantsSDK
//
//  Created by Martin Pastorin on 20/12/2019.
//  Copyright © 2019 Martin Pastorin. All rights reserved.
//

import Foundation

public enum VenueType: String {
    case food
    case drinks
    case coffee
}
