//
//  VenueService.swift
//  RestaurantsSDK
//
//  Created by Martin Pastorin on 20/12/2019.
//  Copyright © 2019 Martin Pastorin. All rights reserved.
//

import Foundation
import ReactiveSwift

struct VenueService {
    // MARK: - Properties
    private let network: NetworkInterface

    // MARK: - Initializer
    init(network: NetworkInterface) {
        self.network = network
    }
}

extension VenueService: VenueServiceInterface {
    func explore(near coordinate: Coordinate2D, type: VenueType) -> ExploreProducer {
        let field = ExploreRequest(coordinate: coordinate, venueType: type)
        let request: Request = .explore(field)
        let producer = network.producer(for: request) as SignalProducer<ExploreResponse, Error>
        return producer
    }

    func details(for venueId: VenueID) -> VenueProducer {
        let request: Request = .details(venueId)
        let producer = network.producer(for: request) as SignalProducer<VenueDetailsResponse, Error>
        return producer.map {
            $0.venue
        }
    }
}
