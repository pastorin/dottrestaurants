//
//  MainInteractor.swift
//  DottRestaurants
//
//  Created by Martin Pastorin on 20/12/2019.
//  Copyright (c) 2019 Martin Pastorin. All rights reserved.
//

protocol MainInteractorInterface {
    func fetchVenues(request: MainModels.FetchVenues.Request)
}

protocol MainDataStore: VenueDetailDataStoreDelegate {
    func venue(for id: SDKVenueID) -> SDKVenue?
}

struct MainData {
    var venues = [SDKVenue]()
}

class MainInteractor: MainInteractorInterface {

    // MARK: - Properties
    private let worker: MainWorkerInterface
    private let presenter: MainPresenterInterface
    var data: MainData

    // MARK: - Init
    init(data: MainData, worker: MainWorkerInterface, presenter: MainPresenterInterface) {
        self.data = data
        self.presenter = presenter
        self.worker = worker
        self.worker.delegate = self
    }

    convenience init(data: MainData, presenter: MainPresenterInterface) {
        let worker = MainWorker()
        self.init(data: data, worker: worker, presenter: presenter)
    }

    // MARK: - MainInteractorInterface
    func fetchVenues(request: MainModels.FetchVenues.Request) {
        guard let userLocation = worker.currentLocation else {
            return
        }

        worker.fetchVenues(near: userLocation, with: .food)
        presentVenues(inProgress: true)
    }

    // MARK: - Helper
    func presentVenues(inProgress: Bool = false, viewport: SDKViewport? = nil) {
        presenter.presentVenues(result: .success(.init(inProgress: inProgress,
                                                       venues: data.venues,
                                                       viewport: viewport)))
    }
}

// MARK: - MainDataStore
extension MainInteractor: MainDataStore {
    func venue(for id: SDKVenueID) -> SDKVenue? {
        data.venues.first {
            $0.id == id
        }
    }
}

// MARK: - VenueDetailDataStoreDelegate
extension MainInteractor {
    func venueDetailDidUpdate(_ venue: SDKVenue) {
        data.venues.updateIfPresent(venue)
    }
}

// MARK: - MainWorkerDelegate
extension MainInteractor: MainWorkerDelegate {
    func workerDidFinishFetchingVenues(_ worker: MainWorkerInterface, with result: MainWorkerInterface.RequestVenuesResult) {
        switch result {
        case .success(let model):
            data.venues = model.venues
            presentVenues(viewport: model.viewport)
        case .failure(let error):
            presenter.presentVenues(result: .failure(error))
        }
    }

    func workerDidUpdateUserLocation(_ worker: MainWorkerInterface) {
        if data.venues.isEmpty {
            fetchVenues(request: .init())
        }
    }
}
