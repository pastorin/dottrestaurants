//
//  String+Extension.swift
//  DottRestaurants
//
//  Created by Martin Pastorin on 22/12/2019.
//  Copyright © 2019 Martin Pastorin. All rights reserved.
//

import Foundation


extension String {
    var isEmptyOrWhitespace: Bool {
        return isEmpty ? true : trimmingCharacters(in: .whitespaces).isEmpty
    }
}

extension Optional where Wrapped == String {
    var isEmptyOrWhitespace: Bool {
        switch self {
        case .none:
            return true
        case .some(let wrapped):
            return wrapped.isEmptyOrWhitespace
        }
    }
}
