//
//  FoursquareSDK.swift
//  RestaurantsSDK
//
//  Created by Martin Pastorin on 20/12/2019.
//  Copyright © 2019 Martin Pastorin. All rights reserved.
//

public final class FoursquareSDK {
    // MARK: - Singleton
    public static let shared = FoursquareSDK()

    // MARK: - Properties
    private(set) var configuration: SDKConfiguration?
    public let venues: VenueServiceInterface

    // MARK: - Initializer
    private init() {
        let network = Network()
        venues = VenueService(network: network)
    }
}

public extension FoursquareSDK {
    static func configure(with configuration: SDKConfiguration) {
        shared.configuration = configuration
    }
}
