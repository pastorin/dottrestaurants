//
//  LocationAccessModule.swift
//  DottRestaurants
//
//  Created by Martin Pastorin on 21/12/2019.
//  Copyright © 2019 Martin Pastorin. All rights reserved.
//

import Foundation
import CoreLocation

class LocationAccessModule: NSObject {
    // MARK: - Singleton
    static var shared = LocationAccessModule()

    // MARK: - Properties
    private let manager = CLLocationManager()
    private let userLocationUpdatePermissionName: Notification.Name = .init(rawValue: "notification.")

    var isLocationEnable: Bool {
        let authorizedOptions: [CLAuthorizationStatus] = [.authorizedAlways, .authorizedWhenInUse]
        let authorizationStatus = CLLocationManager.authorizationStatus()
        return authorizedOptions.contains(authorizationStatus)
    }

    var isLocationAccessDenied: Bool {
        let authorizationStatus = CLLocationManager.authorizationStatus()
        return authorizationStatus == .denied
    }

    private(set) var currentLocation: CLLocation?

    // MARK: - Initializer
    override init() {
        super.init()
        startUpdatingLocation()
    }

    // MARK: - Public
    func checkLocationAccessState() {
        let authorizationStatus = CLLocationManager.authorizationStatus()

        // If user did not authorize yet, request access
        if authorizationStatus == .notDetermined {
            requestLocationAccess()
            return
        }

        startUpdatingLocation()
        SceneSelector.setInitialScene()
    }

    func requestLocationAccess() {
        manager.requestWhenInUseAuthorization()
        manager.delegate = self
    }

    func addObserverForUserLocationUpdate(_ target: AnyObject, selector: Selector) {
        NotificationCenter.default.addObserver(target, selector: selector, name: userLocationUpdatePermissionName, object: nil)
    }

    // MARK: - Helper
    private func startUpdatingLocation() {
        if isLocationEnable {
            manager.startUpdatingLocation()
            manager.delegate = self
        }
    }
}

extension LocationAccessModule: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        checkLocationAccessState()
    }

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let current = locations.max(by: { $0.timestamp > $1.timestamp }) else {
            return
        }

        updateCurrentLocation(with: current)

        manager.stopUpdatingLocation()
    }

    // MARK: - Helper
    private func updateCurrentLocation(with update: CLLocation) {
        guard let previous = currentLocation else {
            currentLocation = update
            broadcastUserLocationUpdate()
            return
        }

        guard update.distance(from: previous) > 50 else {
            print("Ignoring location update, below threshold.")
            return
        }

        currentLocation = update
        broadcastUserLocationUpdate()
    }

    private func broadcastUserLocationUpdate() {
        let notification = Notification(name: userLocationUpdatePermissionName, object: self, userInfo: nil)
        NotificationCenter.default.post(notification)
    }
    
}
