//
//  ReactiveTarget.swift
//  FoursquareSDK
//
//  Created by Martin Pastorin on 20/12/2019.
//  Copyright © 2019 Martin Pastorin. All rights reserved.
//

import Foundation
import ReactiveSwift

open class ReactiveTarget: NSObject, ReactiveExtensionsProvider {
    fileprivate let (lifetime, token) = Lifetime.make()
}

public extension Reactive where Base: ReactiveTarget {
    func makeBindingTarget<U>(on scheduler: Scheduler = UIScheduler(), _ action: @escaping (Base, U) -> Void) -> BindingTarget<U> {
        return BindingTarget(on: scheduler, lifetime: base.lifetime) { [weak base = self.base] value in
            if let base = base {
                action(base, value)
            }
        }
    }
}
