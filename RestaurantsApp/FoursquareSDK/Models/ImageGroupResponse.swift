//
//  ImageGroupResponse.swift
//  FoursquareSDK
//
//  Created by Martin Pastorin on 20/12/2019.
//  Copyright © 2019 Martin Pastorin. All rights reserved.
//

import Foundation

struct ImageGroupResponse: Decodable {
    let images: [Image]
}

// MARK: - Decodable
extension ImageGroupResponse {
    enum CodingKeys: String, CodingKey {
        case items
    }

    init(from decoder: Decoder) throws {
        let response = try decoder.container(keyedBy: CodingKeys.self)
        images = try response.decode(forKey: .items)
    }
}
