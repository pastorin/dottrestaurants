//
//  MainPresenter.swift
//  DottRestaurants
//
//  Created by Martin Pastorin on 20/12/2019.
//  Copyright (c) 2019 Martin Pastorin. All rights reserved.
//

import UIKit

protocol MainPresenterInterface: AnyObject {
    func presentVenues(result: Result<MainModels.FetchVenues.Response, Error>)
}

class MainPresenter: MainPresenterInterface {

    // MARK: - Properties
    private weak var displayer: MainDisplayInterface?

    // MARK: - Init
    init(displayer: MainDisplayInterface) {
        self.displayer = displayer
    }

    // MARK: - MainPresenterInterface
    func presentVenues(result: Result<MainModels.FetchVenues.Response, Error>) {
        let result = result
            .map { MainModels.FetchVenues.ViewModel($0) }
        displayer?.displayVenues(result: result)
    }

}

// MARK: - Models
private extension MainModels.FetchVenues.ViewModel {
    init(_ response: MainModels.FetchVenues.Response) {
        inProgress = response.inProgress
        annotations = response.venues.map { .init($0) }
        region = response.viewport.map { .init(from: $0) }
    }
}
