//
//  MainRouter.swift
//  DottRestaurants
//
//  Created by Martin Pastorin on 20/12/2019.
//  Copyright (c) 2019 Martin Pastorin. All rights reserved.
//

import UIKit

protocol MainRouterInterface: AnyObject {
    func displayProgressView()
    func hideProgressView()
    func display(_ error: Error)
    func displayDetails(for venueId: SDKVenueID)
}

protocol MainDataPassing {
    var dataStore: MainDataStore { get }
}

class MainRouter: MainRouterInterface, MainDataPassing {

    // MARK: - Properties
    let dataStore: MainDataStore
    private weak var sceneViewController: MainViewController?

    // MARK: - Init
    init(viewController: MainViewController, dataStore: MainDataStore) {
        self.sceneViewController = viewController
        self.dataStore = dataStore
    }

    // MARK: - MainRouterInterface
    func displayProgressView() {
        ProgressView.shared.present()
    }

    func hideProgressView() {
        ProgressView.shared.dismiss()
    }

    func display(_ error: Error) {
        let alert = UIAlertController(title: "Error",
                                      message: error.localizedDescription,
                                      preferredStyle: .alert)

        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        sceneViewController?.present(alert, animated: true)
    }

    func displayDetails(for venueId: SDKVenueID) {
        guard let venue = dataStore.venue(for: venueId) else {
            return
        }

        let controller = VenueDetailViewController(data: .init(venue: venue))
        controller.router?.dataStore.delegate = dataStore
        let navigationController = UINavigationController(rootViewController: controller)
        navigationController.applyTheme(.transparent)
        sceneViewController?.show(navigationController, sender: nil)
    }
}

