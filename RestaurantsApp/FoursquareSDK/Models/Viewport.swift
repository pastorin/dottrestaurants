//
//  Viewport.swift
//  FoursquareSDK
//
//  Created by Martin Pastorin on 21/12/2019.
//  Copyright © 2019 Martin Pastorin. All rights reserved.
//

import Foundation

public struct Viewport: Decodable {
    public let northEast: Coordinate2D
    public let southWest: Coordinate2D
}

// MARK: -
public extension Viewport {
    var latitudeDelta: CoordinateDegrees {
        let latitudes = [northEast.latitude, southWest.latitude]
        return latitudes.max()! - latitudes.min()!
    }

    var longitudeDelta: CoordinateDegrees {
        let longitude = [northEast.longitude, southWest.longitude]
        return longitude.max()! - longitude.min()!
    }

    var center: Coordinate2D {
        let latitudes = [northEast.latitude, southWest.latitude]
        let longitude = [northEast.longitude, southWest.longitude]
        return .init(latitude: latitudes.max()! - latitudeDelta / 2,
                     longitude: longitude.max()! - longitudeDelta / 2)
    }
}

// MARK: - Decodable
public extension Viewport {
    enum CodingKeys: String, CodingKey {
        case ne, sw
    }

    init(from decoder: Decoder) throws {
        let response = try decoder.container(keyedBy: CodingKeys.self)

        let northEastWrapper: Coordinate2D_Wrapper = try response.decode(forKey: .ne)
        northEast = northEastWrapper.value

        let southWestWrapper: Coordinate2D_Wrapper = try response.decode(forKey: .sw)
        southWest = southWestWrapper.value
    }
}
