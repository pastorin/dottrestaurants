//
//  Venue.swift
//  RestaurantsSDK
//
//  Created by Martin Pastorin on 20/12/2019.
//  Copyright © 2019 Martin Pastorin. All rights reserved.
//

import Foundation

public typealias VenueID = String
public struct Venue: Decodable, Hashable {
    public let id: VenueID
    public let name: String
    public let description: String?
    public let categories: [Category]
    public let website: URL?
    public let rating: Double?
    public let images: [Image]

    // Location
    public let coordinate: Coordinate2D
    public let formattedAddress: String

    // Contact
    public let phone: String?

    // Likes
    public let likes: Int?
}

// MARK: - Decodable
public extension Venue {
    func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }
}

// MARK: - Decodable
public extension Venue {
    var isDetailed: Bool {
        description != nil
    }
}

// MARK: - Decodable
public extension Venue {
    enum CodingKeys: String, CodingKey {
        case id, name, categories, description, url, rating
        case location, contact, likes, photos
    }

    enum LocationCodingKeys: String, CodingKey {
        case formattedAddress, lat, lng
    }

    enum ContactCodingKeys: String, CodingKey {
        case formattedPhone
    }

    enum LikesCodingKeys: String, CodingKey {
        case count
    }

    enum PhotosCodingKeys: String, CodingKey {
        case groups
    }

    init(from decoder: Decoder) throws {
        let response = try decoder.container(keyedBy: CodingKeys.self)
        let locationResponse = try response.nestedContainer(keyedBy: LocationCodingKeys.self, forKey: .location)
        let photosResponse = try response.nestedContainer(keyedBy: PhotosCodingKeys.self, forKey: .photos)

        id = try response.decode(forKey: .id)
        name = try response.decode(forKey: .name)
        categories = try response.decode(forKey: .categories)
        description = try response.decodeIfPresent(forKey: .description)
        website = try? response.decodeIfPresent(forKey: .url)
        rating = try? response.decodeIfPresent(forKey: .rating)

        // Location
        let address: [String] = try locationResponse.decode(forKey: .formattedAddress)
        formattedAddress = address.joined(separator: ", ")

        let coordinateWrapper: Coordinate2D_Wrapper = try response.decode(forKey: .location)
        coordinate = coordinateWrapper.value

        // Contact
        if response.contains(.contact) {
            let contactResponse = try response.nestedContainer(keyedBy: ContactCodingKeys.self, forKey: .contact)
            phone = try contactResponse.decodeIfPresent(forKey: .formattedPhone)
        } else {
            phone = nil
        }

        // Likes
        if response.contains(.likes) {
            let likesResponse = try response.nestedContainer(keyedBy: LikesCodingKeys.self, forKey: .likes)
            likes = try likesResponse.decodeIfPresent(forKey: .count)
        } else {
            likes = nil
        }

        // Photos
        let groupsResponse: [ImageGroupResponse] = try photosResponse.decode(forKey: .groups)
        images = groupsResponse.reduce(into: []) { result, group in
            result.append(contentsOf: group.images)
        }

    }
}
