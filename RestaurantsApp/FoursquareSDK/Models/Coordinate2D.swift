//
//  Coordinate2D.swift
//  FoursquareSDK
//
//  Created by Martin Pastorin on 21/12/2019.
//  Copyright © 2019 Martin Pastorin. All rights reserved.
//

import CoreLocation

public typealias CoordinateDegrees = CLLocationDegrees
public typealias Coordinate2D = CLLocationCoordinate2D

struct Coordinate2D_Wrapper: Decodable {
    let value: Coordinate2D

    enum CodingKeys: String, CodingKey {
        case lat, lng
    }

    init(from decoder: Decoder) throws {
        let response = try decoder.container(keyedBy: CodingKeys.self)

        let latitude: CLLocationDegrees = try response.decode(forKey: .lat)
        let longitude: CLLocationDegrees = try response.decode(forKey: .lng)

        value = .init(latitude: latitude, longitude: longitude)
    }
}
