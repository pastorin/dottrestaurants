//
//  VenueSocialView.swift
//  DottRestaurants
//
//  Created by Martin Pastorin on 22/12/2019.
//  Copyright © 2019 Martin Pastorin. All rights reserved.
//

import UIKit

protocol VenueSocialViewModel {
    var rating: String { get }
    var likes: String { get }
}

class VenueSocialView: UIView {

    // MARK: - Properties
    private(set) weak var ratingLabel: UILabel!
    private(set) weak var likesLabel: UILabel!

    var viewModel: VenueSocialViewModel? {
        didSet {
            updateContent()
        }
    }

    // MARK: - Initializer
    convenience init() {
        self.init(frame: .zero)
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupUI()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupUI()
    }

    // MARK: - Setup
    private func setupUI() {
        let ratingImageView = UIImageView()
        ratingImageView.image = UIImage(named: "star")
        ratingImageView.tintColor = StyleKit.Color.primary
        ratingImageView.snp.makeConstraints {
            $0.width.equalTo(ratingImageView.snp.height)
        }

        let ratingLabel = UILabel()
        ratingLabel.setStyle(.bold(size: StyleKit.FontSizes.regular,
                                    color: StyleKit.Color.primary))

        let likesImageView = UIImageView()
        likesImageView.image = UIImage(named: "likes")
        likesImageView.tintColor = StyleKit.Color.primary
        likesImageView.snp.makeConstraints {
            $0.width.equalTo(likesImageView.snp.height)
        }

        let likesLabel = UILabel()
        likesLabel.setStyle(.bold(size: StyleKit.FontSizes.regular,
                                  color: StyleKit.Color.primary))
        

        let mainStack = [ratingImageView, ratingLabel, likesImageView, likesLabel]
            .stacked(.horizontal)
            .alignment(.center)
            .spacing(2)
            .spacing(8, after: ratingLabel)
        addSubview(mainStack)
        mainStack.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }

        self.ratingLabel = ratingLabel
        self.likesLabel = likesLabel
    }

    // MARK: Update
    private func updateContent() {
        guard let viewModel = viewModel else {
            defaultContent()
            return
        }

        ratingLabel.text = viewModel.rating
        likesLabel.text = viewModel.likes
    }

    private func defaultContent() {
        ratingLabel.text = nil
        likesLabel.text = nil
    }

}
