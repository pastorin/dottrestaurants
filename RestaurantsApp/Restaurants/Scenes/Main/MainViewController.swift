//
//  MainViewController.swift
//  DottRestaurants
//
//  Created by Martin Pastorin on 20/12/2019.
//  Copyright (c) 2019 Martin Pastorin. All rights reserved.
//

import UIKit
import MapKit
import SnapKit

protocol MainDisplayInterface: AnyObject {
    func displayVenues(result: Result<MainModels.FetchVenues.ViewModel, Error>)
}

class MainViewController: UIViewController {

    // MARK: - Properties
    private(set) weak var mapView: MKMapView!

    var interactor: MainInteractorInterface?
    var router: (MainRouterInterface & MainDataPassing)?

    // MARK: - Init
    init() {
        super.init(nibName: nil, bundle: nil)
        let viewController = self
        let presenter = MainPresenter(displayer: viewController)
        let interactor = MainInteractor(data: .init(), presenter: presenter)
        let router = MainRouter(viewController: viewController, dataStore: interactor)
        viewController.interactor = interactor
        viewController.router = router
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        performFetchVenuesRequest()
    }

    // MARK: - Setup
    private func setupUI() {
        let mapView = MKMapView()
        mapView.showsUserLocation = true
        mapView.delegate = self
        view.addSubview(mapView)
        mapView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }

        self.mapView = mapView
    }

    // MARK: - Helpers
    private func performFetchVenuesRequest() {
        interactor?.fetchVenues(request: .init())
    }
}

// MARK: - MKMapViewDelegate
extension MainViewController: MKMapViewDelegate {

    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        guard let annotation = annotation as? VenueAnnotationViewModel else {
            return nil 
        }

        return MKMarkerAnnotationView(annotation)
    }

    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        guard let venueId = view.reuseIdentifier else {
            return
        }

        router?.displayDetails(for: venueId)
    }
}

// MARK: - MainDisplayInterface
extension MainViewController: MainDisplayInterface {

    func displayVenues(result: Result<MainModels.FetchVenues.ViewModel, Error>) {
        switch result {
        case .success(let model):
            displayProgressView(model.inProgress)
            display(annotations: model.annotations, in: model.region)
        case .failure(let error):
            router?.hideProgressView()
            router?.display(error)
        }
    }

    // MARK: - Helper
    private func display(annotations: [VenueAnnotation], in region: Region? = nil) {
        mapView.addAnnotations(annotations)
        if let region = region {
            mapView.setRegion(region, animated: true)
        }
    }

    private func displayProgressView(_ show: Bool) {
        if show {
            router?.displayProgressView()
        } else {
            router?.hideProgressView()
        }
    }
}
