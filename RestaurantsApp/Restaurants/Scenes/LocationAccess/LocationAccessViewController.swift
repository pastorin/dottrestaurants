//
//  LocationAccessViewController.swift
//  DottRestaurants
//
//  Created by Martin Pastorin on 21/12/2019.
//  Copyright (c) 2019 Martin Pastorin. All rights reserved.
//

import UIKit

protocol LocationAccessDisplayInterface: AnyObject {
    func displayAccessStatus(viewModel: LocationAccessModels.FetchAccessStatus.ViewModel)
}

class LocationAccessViewController: UIViewController {

    // MARK: - Properties
    private(set) weak var cardView: LocationAccessCardView!

    var interactor: LocationAccessInteractorInterface?
    var router: (LocationAccessRouterInterface & LocationAccessDataPassing)?

    // MARK: - Init
    init() {
        super.init(nibName: nil, bundle: nil)
        let viewController = self
        let presenter = LocationAccessPresenter(displayer: viewController)
        let interactor = LocationAccessInteractor(data: .init(), presenter: presenter)
        let router = LocationAccessRouter(viewController: viewController, dataStore: interactor)
        viewController.interactor = interactor
        viewController.router = router
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        performFetchAccessStatusRequest()
    }

    // MARK: - Setup
    private func setupUI() {
        view.backgroundColor = StyleKit.Color.primary

        let cardView = LocationAccessCardView()
        cardView.delegate = self
        view.addSubview(cardView)
        cardView.snp.makeConstraints {
            $0.leading.trailing.equalToSuperview().inset(24)
            $0.center.equalToSuperview()
        }

        self.cardView = cardView
    }

    // MARK: - Helpers
    private func performFetchAccessStatusRequest() {
        interactor?.fetchAccessStatus(request: .init())
    }
}

// MARK: - LocationAccessCardDelegate
extension LocationAccessViewController: LocationAccessCardDelegate {
    func locationAccessCardViewDidTapButton(_ cell: LocationAccessCardView) {
        interactor?.requestAccessStatus(request: .init())
    }
}

// MARK: - LocationAccessDisplayInterface
extension LocationAccessViewController: LocationAccessDisplayInterface {

    func displayAccessStatus(viewModel: LocationAccessModels.FetchAccessStatus.ViewModel) {
        cardView.viewModel = viewModel
    }
}
