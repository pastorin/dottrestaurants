//
//  UILabel+Extension.swift
//  DottRestaurants
//
//  Created by Martin Pastorin on 21/12/2019.
//  Copyright © 2019 Martin Pastorin. All rights reserved.
//

import UIKit

extension  UILabel {
    enum LabelStyle {
        case regular(size: CGFloat, color: UIColor)
        case light(size: CGFloat, color: UIColor)
        case bold(size: CGFloat, color: UIColor)
        case medium(size: CGFloat, color: UIColor)
    }

    func setStyle(_ style: LabelStyle) {
        switch style {
        case let .regular(size, color):
            font = StyleKit.Font.regular(size: size)
            textColor = color
        case let .light(size, color):
            font = StyleKit.Font.light(size: size)
            textColor = color
        case let .bold(size, color):
            font = StyleKit.Font.bold(size: size)
            textColor = color
        case let .medium(size, color):
            font = StyleKit.Font.medium(size: size)
            textColor = color
        }
        lineBreakMode = .byTruncatingTail
    }
}
