//
//  LocationAccessWorker.swift
//  DottRestaurants
//
//  Created by Martin Pastorin on 21/12/2019.
//  Copyright (c) 2019 Martin Pastorin. All rights reserved.
//

import Foundation
import ReactiveSwift

protocol LocationAccessWorkerInterface: AnyObject {
    var delegate: LocationAccessWorkerDelegate? { get set }

    var isLocationAccessDenied: Bool { get }
    func requestLocationAccess()
    func openAppSettings()
}

protocol LocationAccessWorkerDelegate: AnyObject {

}

class LocationAccessWorker: SDKReactiveTarget, LocationAccessWorkerInterface {

    // MARK: - Properties
    weak var delegate: LocationAccessWorkerDelegate?

    var isLocationAccessDenied: Bool {
        LocationAccessModule.shared.isLocationAccessDenied
    }

    // MARK: - LocationAccessWorkerInterface
    func requestLocationAccess() {
        LocationAccessModule.shared.requestLocationAccess()
    }

    func openAppSettings() {
        guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
            return
        }
        UIApplication.shared.open(settingsUrl)
    }
}

