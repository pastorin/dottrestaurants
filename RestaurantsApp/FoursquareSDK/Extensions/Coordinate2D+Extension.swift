//
//  Coordinate2D+Extension.swift
//  FoursquareSDK
//
//  Created by Martin Pastorin on 21/12/2019.
//  Copyright © 2019 Martin Pastorin. All rights reserved.
//

import Foundation

extension Coordinate2D: Equatable {
    public static func == (lhs: Coordinate2D, rhs: Coordinate2D) -> Bool {
        return lhs.latitude == rhs.latitude
            && lhs.longitude == rhs.longitude
    }
}
