//
//  Network.swift
//  RestaurantsSDK
//
//  Created by Martin Pastorin on 20/12/2019.
//  Copyright © 2019 Martin Pastorin. All rights reserved.
//

import Foundation
import Alamofire
import ReactiveSwift

protocol NetworkInterface {
    func producer<Target: Decodable>(for apiRequest: APIRequest) ->  SignalProducer<Target, Error>
}

struct Network: NetworkInterface {
    // MARK: - Properties
    private let session: SessionManager

    // MARK: - Initializer
    private init(_ session: SessionManager) {
        session.startRequestsImmediately = false
        self.session = session
    }

    init() {
        let configuration = URLSessionConfiguration.default
        configuration.httpAdditionalHeaders = SessionManager.defaultHTTPHeaders
        let session = SessionManager(configuration: configuration)
        self.init(session)
    }
}

// MARK: - Producers
extension Network {
    func producer<Target: Decodable>(for apiRequest: APIRequest) ->  SignalProducer<Target, Error> {
        return session
            .request(from: apiRequest)
            .mapError { $0 as Error }
            .attemptMap { try $0.decode() }
    }
}
