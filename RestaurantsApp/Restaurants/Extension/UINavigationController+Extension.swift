//
//  UINavigationController+Extension.swift
//  DottRestaurants
//
//  Created by Martin Pastorin on 22/12/2019.
//  Copyright © 2019 Martin Pastorin. All rights reserved.
//

import UIKit

enum NavigationControllerTheme: Int {
    case normal
    case transparent
}

extension UINavigationController {

    func applyTheme(_ theme: NavigationControllerTheme) {
        switch theme {
        case .normal:
            navigationBar.barTintColor = StyleKit.Color.primary
            navigationBar.isHidden = false
            navigationBar.isTranslucent = false

            navigationBar.tintColor = StyleKit.Color.white
            navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: StyleKit.Color.white]
            navigationBar.shadowImage = UIImage()
        case .transparent:
            navigationBar.isHidden = false
            navigationBar.isTranslucent = true
            view.backgroundColor = .clear
            navigationBar.setBackgroundImage(UIImage(), for: .default)

            navigationBar.tintColor = StyleKit.Color.white
            navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: StyleKit.Color.white]
            navigationBar.shadowImage = UIImage()
        }
    }
}
