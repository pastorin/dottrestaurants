//
//  VenueDetailRouter.swift
//  DottRestaurants
//
//  Created by Martin Pastorin on 22/12/2019.
//  Copyright (c) 2019 Martin Pastorin. All rights reserved.
//

import UIKit

protocol VenueDetailRouterInterface: AnyObject {

}

protocol VenueDetailDataPassing {
    var dataStore: VenueDetailDataStore { get }
}

class VenueDetailRouter: VenueDetailRouterInterface, VenueDetailDataPassing {

    // MARK: - Properties
    let dataStore: VenueDetailDataStore
    private weak var sceneViewController: VenueDetailViewController?

    // MARK: - Init
    init(viewController: VenueDetailViewController, dataStore: VenueDetailDataStore) {
        self.sceneViewController = viewController
        self.dataStore = dataStore
    }

    // MARK: - VenueDetailRouterInterface
}
