//
//  VenueContactView.swift
//  DottRestaurants
//
//  Created by Martin Pastorin on 22/12/2019.
//  Copyright © 2019 Martin Pastorin. All rights reserved.
//

import UIKit

protocol VenueContactViewModel {
    var address: String { get }
    var website: String { get }
    var phone: String { get }
}

class VenueContactView: UIView {

    // MARK: - Properties
    private(set) weak var addressRow: VenueContactRowView!
    private(set) weak var websiteRow: VenueContactRowView!
    private(set) weak var phoneRow: VenueContactRowView!

    var viewModel: VenueContactViewModel? {
        didSet {
            updateContent()
        }
    }

    // MARK: - Initializer
    convenience init() {
        self.init(frame: .zero)
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupUI()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupUI()
    }

    // MARK: - Setup
    private func setupUI() {
        let addressRow = VenueContactRowView()
        let websiteRow = VenueContactRowView()
        let phoneRow = VenueContactRowView()

        let mainStack = [addressRow, websiteRow, phoneRow]
            .stacked(.vertical)
            .spacing(16)
        addSubview(mainStack)
        mainStack.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }

        self.addressRow = addressRow
        self.websiteRow = websiteRow
        self.phoneRow = phoneRow
    }

    // MARK: Update
    private func updateContent() {
        guard let viewModel = viewModel else {
            defaultContent()
            return
        }

        addressRow.viewModel = VenueContactRowViewItem(image: UIImage(named: "location"), title: viewModel.address)
        websiteRow.viewModel = VenueContactRowViewItem(image: UIImage(named: "website"), title: viewModel.website)
        phoneRow.viewModel = VenueContactRowViewItem(image: UIImage(named: "phone"), title: viewModel.phone)
    }

    private func defaultContent() {
        addressRow.viewModel = nil
        websiteRow.viewModel = nil
        phoneRow.viewModel = nil
    }

}
