//
//  SceneSelector.swift
//  DottRestaurants
//
//  Created by Martin Pastorin on 20/12/2019.
//  Copyright © 2019 Martin Pastorin. All rights reserved.
//

import UIKit

struct SceneSelector {

    // MARK: - Singleton
    static var shared = SceneSelector()

    // MARK: - Properties
    private(set) var mainWindow: UIWindow? {
        didSet {
            mainWindow?.makeKey()
        }
    }

    static func setInitialScene() {
        if !LocationAccessModule.shared.isLocationEnable {
            setScene(fromViewController: LocationAccessViewController.self)
            return
        }

        setScene(fromViewController: MainViewController.self)
    }

    static func setScene(fromViewController viewControllerType: UIViewController.Type, inNavigationController showNavigationController: Bool = false) {
        let viewController = viewControllerType.init()

        let nextViewController: UIViewController = {
            if showNavigationController {
                return UINavigationController(rootViewController: viewController)
            } else {
                return viewController
            }
        }()

        present(viewController: nextViewController)
    }

    static func setWindow(_ window: UIWindow?) {
        guard let window = window else {
            return
        }

        SceneSelector.shared.mainWindow = window
    }

    // MARK: - Helpers
    private static func present(viewController: UIViewController, in window: UIWindow? = SceneSelector.shared.mainWindow) {
        guard let window = window else {
            fatalError("Usage of the SceneSelector requires a window to be set through the setWindow() method")
        }

        // Clean up the view stack before resetting the rootViewController
        for subview in window.subviews {
            subview.removeFromSuperview()
        }

        let transition = CATransition.fade()
        window.layer.add(transition, forKey: nil)

        window.rootViewController = viewController
    }
}
