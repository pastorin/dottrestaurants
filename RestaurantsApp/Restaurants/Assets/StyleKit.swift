//
//  StyleKit.swift
//  DottRestaurants
//
//  Created by Martin Pastorin on 21/12/2019.
//  Copyright © 2019 Martin Pastorin. All rights reserved.
//

import UIKit
import Kingfisher

enum StyleKit {

    enum Font {
        enum FontName {
            static let customFontRegular = "Roboto-Regular"
            static let customFontBold = "Roboto-Bold"
            static let customFontMedium = "Roboto-Medium"
            static let customFontLight = "Roboto-Light"
        }

        static func light(size: CGFloat) -> UIFont {
            return UIFont(name: FontName.customFontLight, size: size)!
        }

        static func regular(size: CGFloat) -> UIFont {
            return UIFont(name: FontName.customFontRegular, size: size)!
        }

        static func bold(size: CGFloat) -> UIFont {
            return UIFont(name: FontName.customFontBold, size: size)!
        }

        static func medium(size: CGFloat) -> UIFont {
            return UIFont(name: FontName.customFontMedium, size: size)!
        }
    }

    enum FontSizes {
        /// Size 10
        static let xsmall: CGFloat = 10
        /// Size 12
        static let small: CGFloat = 12
        /// Size 14
        static let regular: CGFloat = 14
        /// Size 20
        static let large: CGFloat = 18
        /// Size 32
        static let xlarge: CGFloat = 32
    }

    enum Color {
        static let primary = UIColor(hex: 0x12918E)
//        static let secondary =

        static let background = UIColor(hex: 0xF9F9F9)
        static let background1 = UIColor(hex: 0xFFFFFF)

        static let label = UIColor(hex: 0x113260)
        static let label1 = UIColor(hex: 0x6F7B91)
        static let label2 = UIColor(hex: 0x999999)

        static let black = UIColor(hex: 0x000000)
        static let black40 = UIColor(hex: 0x000000, alpha: StyleKit.Alpha.heavy)
        static let black70 = UIColor(hex: 0x000000, alpha: StyleKit.Alpha.regular)

        static let white = UIColor(hex: 0xFFFFFF)
        static let white70 = UIColor(hex: 0xFFFFFF, alpha: StyleKit.Alpha.regular)
    }

    enum CornerRadius {
        /// Radius 3.0
        static let slightly: CGFloat = 3.0
        /// Radius 8.0
        static let regular: CGFloat = 8.0
        /// Radius 12.0
        static let heavy: CGFloat = 12.0
    }

    enum Alpha {
        /// Alpha 0.9
        static let slightly: CGFloat = 0.9
        /// Alpha 0.7
        static let regular: CGFloat = 0.7
        /// Alpha 0.4
        static let heavy: CGFloat = 0.4
    }

    enum Animation {
        static let duration: TimeInterval = 0.3
        static let damping: CGFloat = 0.4
        static let spring: CGFloat = 0.7
    }

    enum KingfisherOptions {
        static let imageFadeTransition = KingfisherOptionsInfoItem.transition(.fade(Animation.duration))
    }
}
