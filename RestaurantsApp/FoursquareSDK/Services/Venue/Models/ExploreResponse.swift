//
//  ExploreResponse.swift
//  RestaurantsSDK
//
//  Created by Martin Pastorin on 20/12/2019.
//  Copyright © 2019 Martin Pastorin. All rights reserved.
//

import Foundation

public struct ExploreResponse: Decodable {
    public let venues: [Venue]
    public let viewport: Viewport
}

// MARK: - Decodable
public extension ExploreResponse {
    enum CodingKeys: String, CodingKey {
        case response
    }

    enum ResponseKeys: String, CodingKey {
        case groups, suggestedBounds
    }

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let response = try container.nestedContainer(keyedBy: ResponseKeys.self, forKey: .response)

        let groupsResponse: [VenueGroupResponse] = try response.decode(forKey: .groups)
        venues = groupsResponse.reduce(into: []) { result, group in
            result.append(contentsOf: group.venues)
        }

        viewport = try response.decode(forKey: .suggestedBounds)
    }
}
