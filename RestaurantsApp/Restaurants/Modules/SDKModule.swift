//
//  SDKModule.swift
//  RestaurantsSDK
//
//  Created by Martin Pastorin on 20/12/2019.
//  Copyright © 2019 Martin Pastorin. All rights reserved.
//

import Foundation
import FoursquareSDK

typealias SDKReactiveTarget = ReactiveTarget
typealias SDKVenueID = VenueID
typealias SDKVenue = Venue
typealias SDKVenueType = VenueType
typealias SDKImage = Image
typealias SDKCoordinate2D = Coordinate2D
typealias SDKViewport = Viewport
typealias SDKExploreResponse = ExploreResponse

// MARK: - Configuration
struct Configuration: SDKConfiguration {
    var serviceURL: URL {
        do {
            return try Environment.value(for: .apiURL).asURL()
        } catch {
            fatalError("Missing service URL")
        }
    }

    var clientId: String {
        return Environment.value(for: .foursquareClientId)
    }

    var clientSecret: String {
        return Environment.value(for: .foursquareClientSecret)
    }
}

enum SDKModule  {
    // MARK: - Properties
    static var shared: FoursquareSDK {
        return FoursquareSDK.shared
    }

    // MARK: - Public
    static func configure(with configuration: SDKConfiguration = Configuration()) {
        FoursquareSDK.configure(with: configuration)
    }
}
