//
//  Array+Extension.swift
//  DottRestaurants
//
//  Created by Martin Pastorin on 21/12/2019.
//  Copyright © 2019 Martin Pastorin. All rights reserved.
//

import Foundation

public extension Array where Element: Hashable {

    mutating func updateIfPresent(_ other: Element) {
        guard let index = firstIndex(where: { $0.hashValue == other.hashValue }) else {
            return
        }
        self[index] = other
    }
}
