//
//  Category.swift
//  RestaurantsSDK
//
//  Created by Martin Pastorin on 20/12/2019.
//  Copyright © 2019 Martin Pastorin. All rights reserved.
//

import Foundation

public struct Category: Decodable, Equatable {
    public let id: String
    public let name: String
    public var primary: Bool
    public var icon: URL?
}

// MARK: - Decodable
public extension Category {
    enum CodingKeys: String, CodingKey {
        case id, name, primary, icon
    }

    enum IconCodingKeys: String, CodingKey {
        case prefix, suffix
    }

    init(from decoder: Decoder) throws {
        let response = try decoder.container(keyedBy: CodingKeys.self)
        let iconResponse = try response.nestedContainer(keyedBy: IconCodingKeys.self, forKey: .icon)

        id = try response.decode(forKey: .id)
        name = try response.decode(forKey: .name)
        primary = try response.decodeIfPresent(forKey: .primary) ?? false

        let prefix: String = try iconResponse.decode(forKey: .prefix)
        let suffix: String = try iconResponse.decode(forKey: .suffix)
        icon = URL(string: prefix + suffix)
    }

}
