//
//  UIView+Extension.swift
//  DottRestaurants
//
//  Created by Martin Pastorin on 22/12/2019.
//  Copyright © 2019 Martin Pastorin. All rights reserved.
//

import UIKit

extension UIView {

    static var spacing: UIView {
        let view = UIView()
        view.setContentCompressionResistancePriority(.defaultLow, for: .horizontal)
        view.setContentCompressionResistancePriority(.defaultLow, for: .vertical)
        view.setContentHuggingPriority(.defaultLow, for: .horizontal)
        view.setContentHuggingPriority(.defaultLow, for: .vertical)
        return view
    }
}
