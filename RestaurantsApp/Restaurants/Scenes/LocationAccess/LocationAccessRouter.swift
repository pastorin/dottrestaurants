//
//  LocationAccessRouter.swift
//  DottRestaurants
//
//  Created by Martin Pastorin on 21/12/2019.
//  Copyright (c) 2019 Martin Pastorin. All rights reserved.
//

import UIKit

protocol LocationAccessRouterInterface: AnyObject {

}

protocol LocationAccessDataPassing {
    var dataStore: LocationAccessDataStore { get }
}

class LocationAccessRouter: LocationAccessRouterInterface, LocationAccessDataPassing {

    // MARK: - Properties
    let dataStore: LocationAccessDataStore
    private weak var sceneViewController: LocationAccessViewController?

    // MARK: - Init
    init(viewController: LocationAccessViewController, dataStore: LocationAccessDataStore) {
        self.sceneViewController = viewController
        self.dataStore = dataStore
    }

    // MARK: - LocationAccessRouterInterface
}
