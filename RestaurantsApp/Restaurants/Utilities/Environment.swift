//
//  Environment.swift
//  DottRestaurants
//
//  Created by Martin Pastorin on 20/12/2019.
//  Copyright © 2019 Martin Pastorin. All rights reserved.
//

import Foundation

struct Environment {
    enum VariableNames: String {
        case apiURL = "API_URL"
        case foursquareClientId = "FOURSQUARE_CLIENT_ID"
        case foursquareClientSecret = "FOURSQUARE_CLIENT_SECRET"
    }

    /// Requests the value for the given environment variable key name, defined in the target's
    /// Build Settings > User-Defined settings.
    /// To add a new environment variable:
    /// 1. Define the key name in the User-Defined settings, along with the value
    /// 2. Add the key name to the Info.plist (see API_URL as an example)
    ///
    /// - Parameter name: The name of the key as stated in the User-Defined settings
    /// - Returns: A string representation of the value requested
    static func value(for name: VariableNames) -> String {
        // We're implicitly unwrapping here, because we want it to break if used wrongly
        let environmentDictionary = Bundle.main.infoDictionary!
        return environmentDictionary[name.rawValue] as! String
    }
}

