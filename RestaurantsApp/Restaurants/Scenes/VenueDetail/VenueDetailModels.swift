//
//  VenueDetailModels.swift
//  DottRestaurants
//
//  Created by Martin Pastorin on 22/12/2019.
//  Copyright (c) 2019 Martin Pastorin. All rights reserved.
//

import Foundation

enum VenueDetailModels {
    enum FetchVenue { }
}

extension VenueDetailModels.FetchVenue {

    struct Request { }

    struct Response {
        let venue: SDKVenue
    }

    struct ViewModel: VenueInfoViewModel, VenueContactViewModel {
        let imageUrl: URL?
        let title: String
        let subtitle: String
        let description: String?
        let rating: String
        let likes: String
        let address: String
        let website: String
        let phone: String
    }
}
