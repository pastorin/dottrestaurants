//
//  KeyedDecodingContainer+Extension.swift
//  RestaurantsSDK
//
//  Created by Martin Pastorin on 20/12/2019.
//  Copyright © 2019 Martin Pastorin. All rights reserved.
//

import Foundation

extension KeyedDecodingContainer {

    func decode<T: Decodable>(forKey key: Key) throws  -> T {
        return try self.decode(T.self, forKey: key)
    }

    func decodeIfPresent<T: Decodable>(forKey key: Key) throws  -> T? {
        return try self.decodeIfPresent(T.self, forKey: key)
    }

    func decodeArray<T: Decodable>(forKey key: Key) throws  -> [T] {
        return try self.decodeIfPresent([T].self, forKey: key) ?? []
    }
}
