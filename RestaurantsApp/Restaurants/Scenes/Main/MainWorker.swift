//
//  MainWorker.swift
//  DottRestaurants
//
//  Created by Martin Pastorin on 20/12/2019.
//  Copyright (c) 2019 Martin Pastorin. All rights reserved.
//

import Foundation
import ReactiveSwift
import CoreLocation

protocol MainWorkerInterface: AnyObject {
    typealias RequestVenuesResult = Result<SDKExploreResponse, Error>

    var delegate: MainWorkerDelegate? { get set }
    var currentLocation: SDKCoordinate2D? { get}

    func fetchVenues(near coordinate: SDKCoordinate2D, with type: SDKVenueType)
    func checkLocationAccessState()
}

protocol MainWorkerDelegate: AnyObject {
    func workerDidFinishFetchingVenues(_ worker: MainWorkerInterface, with result: MainWorkerInterface.RequestVenuesResult)
    func workerDidUpdateUserLocation(_ worker: MainWorkerInterface)
}

class MainWorker: SDKReactiveTarget, MainWorkerInterface {
    // MARK: Aliases
    typealias FetchResult = Result<Void, Error>

    // MARK: - Properties
    weak var delegate: MainWorkerDelegate?

    var currentLocation: SDKCoordinate2D? {
        return LocationAccessModule.shared.currentLocation?.coordinate
    }

    // MARK: - Initializer
    override init() {
        super.init()
        LocationAccessModule.shared.addObserverForUserLocationUpdate(self, selector: #selector(updateLatestLocation))
    }

    // MARK: - MainWorkerInterface
    func fetchVenues(near coordinate: SDKCoordinate2D, with type: SDKVenueType) {
        let producer = SDKModule.shared
            .venues
            .explore(near: coordinate, type: type)
            .materializeResults()
        reactive.venuesRequestResult <~ producer
    }

    func checkLocationAccessState() {
        LocationAccessModule.shared.checkLocationAccessState()
    }

    // MARK: - Helpers
    @objc
    private func updateLatestLocation() {
        delegate?.workerDidUpdateUserLocation(self)
    }
}

// MARK: - Reactive
private extension Reactive where Base: MainWorker {
    var venuesRequestResult: BindingTarget<Base.RequestVenuesResult> {
        return makeBindingTarget { worker, result in
            worker.delegate?.workerDidFinishFetchingVenues(worker, with: result)
        }
    }
}
