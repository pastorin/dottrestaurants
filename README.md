# Dott Restaurants App

## General Info

### Project description

Small prototype which shows in a map Foursquare's recommended restaurants around the user and display also its details.

This prototype consists of three screens:

* First screen: Screen to request location permissions to fetch the user location.

* Second screen: A map using Apple Maps displaying restaurants around the user.

* Third screen: Detail screen which displays image, name, category, description, rating and other information of the restaurant. 

### Technology stack 
* Language - Swift 5
* Deployment version - iOS 13.2+
* IDE - Xcode 
* Tools - CocoaPods
* Open Source Libraries - SnapKit, SwiftMessages, Kingfisher, Alamofire, ReactiveSwift, AlamofireActivityLogger

## How to setup
* Checkout the latest version of the code with the *git clone* command
* Ensure that you have CocoaPods installed on your machine (See the installation manual [here](https://cocoapods.org/))
* Create a developer user in [Foursquare](https://foursquare.com/developers/signup) and obtain your Client ID and Secret (See [Getting Started](https://developer.foursquare.com/docs/api) for more information)
* Go to `Build Settings` in the `DottRestaurant` target and replace the values in `FOURSQUARE_CLIENT_ID ` and `FOURSQUARE_CLIENT_SECRET`.
* Optional: Install your personal certificates to run the application on a physical device. Otherwise use the simulators.


## Development Phase

### Architecture
The app was built using Clean Swift architecture. For more information, see the [Clean Swift website](https://clean-swift.com)

### Testing strategy
Clean Architecture splits up the application into Scenes, each with its own VIP-Cycle. This cycle contains components, each with one single responsibility. These components define their inputs/outputs using protocols, making the testing of the app easier and more straightforward.


## Contact
* Martin Pastorin - [Linkedin](https://linkedin.com/in/mpastorin)