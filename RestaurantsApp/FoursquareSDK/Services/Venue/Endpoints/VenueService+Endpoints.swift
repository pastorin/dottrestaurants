//
//  VenueService+Endpoints.swift
//  RestaurantsSDK
//
//  Created by Martin Pastorin on 20/12/2019.
//  Copyright © 2019 Martin Pastorin. All rights reserved.
//

import Foundation

extension VenueService {
    enum Request: APIRequest {
        case explore(ExploreRequest)
        case details(VenueID)
    }
}

extension VenueService.Request {
    var method: APIHTTPMethod {
            return .get
        }

    var path: String {
        switch self {
        case .explore:
            return APIConstants.Paths.vanueExplore
        case .details(let id):
            return APIConstants.Paths.vanueDetail(id)
        }
    }

    var parameters: APIParameters? {
        switch self {
        case .explore(let fields):
            return fields.encoded()
        case .details:
            return nil
        }
    }
    
    var encoding: ParameterEncoding {
        return URLEncoding.default
    }
}
