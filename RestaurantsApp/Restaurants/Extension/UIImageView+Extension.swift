//
//  UIImageView+Extension.swift
//  FoursquareSDK
//
//  Created by Martin Pastorin on 22/12/2019.
//  Copyright © 2019 Martin Pastorin. All rights reserved.
//

import UIKit
import Kingfisher

extension UIImageView {

    // MARK: - Alias
    typealias Options = KingfisherOptionsInfo
    typealias CompletionBlock = (Result<UIImage, Error>) -> Void

    // MARK: - Properties
    var renderOptions: Options {
        return [StyleKit.KingfisherOptions.imageFadeTransition]
    }

    // MARK: - Methods
    func setResource(_ resource: Resource?, placeholder: UIImage? = nil, completion: CompletionBlock? = nil) {
        kf.setImage(with: resource, placeholder: placeholder, options: renderOptions) { result in
            let result = result
                .map { $0.image }
                .mapError { $0 as Swift.Error }
            completion?(result)
        }
    }
}
