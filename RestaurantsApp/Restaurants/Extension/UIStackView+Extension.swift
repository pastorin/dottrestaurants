//
//  UIStackView+Extension.swift
//  DottRestaurants
//
//  Created by Martin Pastorin on 21/12/2019.
//  Copyright © 2019 Martin Pastorin. All rights reserved.
//

import UIKit

extension Array where Element: UIView {

    func stacked(_ axis: NSLayoutConstraint.Axis) -> UIStackView {
        let stack = UIStackView(arrangedSubviews: self)
        stack.axis = axis
        return stack
    }
}

extension UIStackView {

    func spacing(_ value: CGFloat) -> UIStackView {
        spacing = value
        return self
    }

    func spacing(_ value: CGFloat, after view: UIView) -> UIStackView {
        setCustomSpacing(value, after: view)
        return self
    }

    func alignment(_ value: Alignment) -> UIStackView {
        alignment = value
        return self
    }

    func distribution(_ value: Distribution) -> UIStackView {
        distribution = value
        return self
    }

    func layoutMargins(top: CGFloat = 0, left: CGFloat = 0, bottom: CGFloat = 0, right: CGFloat = 0) -> UIStackView {
        layoutMargins = UIEdgeInsets(top: top, left: left, bottom: bottom, right: right)
        isLayoutMarginsRelativeArrangement = true
        insetsLayoutMarginsFromSafeArea = false
        return self
    }

    func removeArrangedSubviews() {
        arrangedSubviews.forEach {
            removeArrangedSubview($0)
        }
    }
}
