//
//  VenueDetailsResponse.swift
//  FoursquareSDK
//
//  Created by Martin Pastorin on 20/12/2019.
//  Copyright © 2019 Martin Pastorin. All rights reserved.
//

import Foundation

struct VenueDetailsResponse: Decodable {
    let venue: Venue
}

// MARK: - Decodable
extension VenueDetailsResponse {
    enum CodingKeys: String, CodingKey {
        case response
    }

    enum ResponseKeys: String, CodingKey {
        case venue
    }

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let response = try container.nestedContainer(keyedBy: ResponseKeys.self, forKey: .response)
        venue = try response.decode(forKey: .venue)
    }
}
