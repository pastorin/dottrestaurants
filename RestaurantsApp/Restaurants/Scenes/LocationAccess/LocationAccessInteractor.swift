//
//  LocationAccessInteractor.swift
//  DottRestaurants
//
//  Created by Martin Pastorin on 21/12/2019.
//  Copyright (c) 2019 Martin Pastorin. All rights reserved.
//

protocol LocationAccessInteractorInterface {
    func fetchAccessStatus(request: LocationAccessModels.FetchAccessStatus.Request)
    func requestAccessStatus(request: LocationAccessModels.RequestAccessStatus.Request)
}

protocol LocationAccessDataStore: AnyObject {

}

struct LocationAccessData {

}

class LocationAccessInteractor: LocationAccessInteractorInterface {

    // MARK: - Properties
    private let worker: LocationAccessWorkerInterface
    private let presenter: LocationAccessPresenterInterface
    var data: LocationAccessData

    // MARK: - Init
    init(data: LocationAccessData, worker: LocationAccessWorkerInterface, presenter: LocationAccessPresenterInterface) {
        self.data = data
        self.presenter = presenter
        self.worker = worker
        self.worker.delegate = self
    }

    convenience init(data: LocationAccessData, presenter: LocationAccessPresenterInterface) {
        let worker = LocationAccessWorker()
        self.init(data: data, worker: worker, presenter: presenter)
    }

    func requestAccessStatus(request: LocationAccessModels.RequestAccessStatus.Request) {
        if !worker.isLocationAccessDenied {
            worker.requestLocationAccess()
        } else {
            worker.openAppSettings()
        }
    }

    // MARK: - LocationAccessInteractorInterface
    func fetchAccessStatus(request: LocationAccessModels.FetchAccessStatus.Request) {
        presenter.presentAccessStatus(response: .init(accessDenied: worker.isLocationAccessDenied))
    }
}

// MARK: - LocationAccessDataStore
extension LocationAccessInteractor: LocationAccessDataStore {

}

// MARK: - LocationAccessWorkerDelegate
extension LocationAccessInteractor: LocationAccessWorkerDelegate {
    
}
