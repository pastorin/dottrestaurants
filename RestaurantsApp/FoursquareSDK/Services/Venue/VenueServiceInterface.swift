//
//  VenueServiceInterface.swift
//  RestaurantsSDK
//
//  Created by Martin Pastorin on 20/12/2019.
//  Copyright © 2019 Martin Pastorin. All rights reserved.
//

import Foundation
import ReactiveSwift

public protocol VenueServiceInterface {
    // MARK: - Aliases
    typealias ExploreProducer = SignalProducer<ExploreResponse, Error>
    typealias VenueProducer = SignalProducer<Venue, Error>

    // MARK: - Public
    func explore(near coordinate: Coordinate2D, type: VenueType) -> ExploreProducer
    func details(for venueId: VenueID) -> VenueProducer
}
