//
//  LocationAccessModels.swift
//  DottRestaurants
//
//  Created by Martin Pastorin on 21/12/2019.
//  Copyright (c) 2019 Martin Pastorin. All rights reserved.
//

enum LocationAccessModels {
    enum FetchAccessStatus { }
    enum RequestAccessStatus { }
}

extension LocationAccessModels.FetchAccessStatus {

    struct Request { }

    struct Response {
        let accessDenied: Bool
    }

    struct ViewModel: LocationAccessCardViewModel {
        let title: String
        let description: String
        let buttonTitle: String
    }
}

extension LocationAccessModels.RequestAccessStatus {

    struct Request {

    }

    struct Response {

    }

    struct ViewModel {

    }
}
