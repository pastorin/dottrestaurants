//
//  VenueDetailPresenter.swift
//  DottRestaurants
//
//  Created by Martin Pastorin on 22/12/2019.
//  Copyright (c) 2019 Martin Pastorin. All rights reserved.
//

import UIKit
import Foundation

protocol VenueDetailPresenterInterface: AnyObject {
    func presentVenue(response: VenueDetailModels.FetchVenue.Response)
}

class VenueDetailPresenter: VenueDetailPresenterInterface {

    // MARK: - Properties
    private weak var displayer: VenueDetailDisplayInterface?

    // MARK: - Init
    init(displayer: VenueDetailDisplayInterface) {
        self.displayer = displayer
    }

    // MARK: - VenueDetailPresenterInterface
    func presentVenue(response: VenueDetailModels.FetchVenue.Response) {
        displayer?.displayVenue(viewModel: .init(response))
    }
}

// MARK: - Models
extension VenueDetailModels.FetchVenue.ViewModel {
    init(_ response: VenueDetailModels.FetchVenue.Response) {
        let venue = response.venue
        imageUrl = venue.images.first.map { $0.url }
        title = venue.name
        subtitle = venue.categories
            .map { $0.name }
            .filter { !$0.isEmptyOrWhitespace }
            .joined(separator: " • ")
        description = venue.description
        rating = venue.rating
            .map { $0 }
            .map { .init($0) } ?? "N/A"

        likes = venue.likes
            .map { $0 }
            .map { .init($0) } ?? "N/A"

        address = venue.formattedAddress
        website = venue.website?.absoluteString ?? "N/A"
        phone = venue.phone ?? "N/A"
    }
}
