//
//  LocationAccessCardView.swift
//  DottRestaurants
//
//  Created by Martin Pastorin on 21/12/2019.
//  Copyright © 2019 Martin Pastorin. All rights reserved.
//

import UIKit

protocol LocationAccessCardViewModel {
    var title: String { get }
    var description: String { get }
    var buttonTitle: String { get }
}

protocol LocationAccessCardDelegate: AnyObject {
    func locationAccessCardViewDidTapButton(_ cell: LocationAccessCardView)
}

class LocationAccessCardView: UIView {

    // MARK: - Properties
    private(set) weak var imageView: UIImageView!
    private(set) weak var titleLabel: UILabel!
    private(set) weak var descriptionLabel: UILabel!
    private(set) weak var primaryButton: UIButton!

    weak var delegate: LocationAccessCardDelegate?

    var viewModel: LocationAccessCardViewModel? {
        didSet {
            updateContent()
        }
    }

    // MARK: - Initializer
    convenience init() {
        self.init(frame: .zero)
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupUI()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupUI()
    }

    // MARK: - Setup
    private func setupUI() {
        backgroundColor = StyleKit.Color.white
        layer.cornerRadius = StyleKit.CornerRadius.heavy

        let imageView = UIImageView()
        imageView.image = UIImage(named: "location_access")
        imageView.snp.makeConstraints {
            $0.height.width.equalTo(96)
        }

        let titleLabel = UILabel()
        titleLabel.textAlignment = .center
        titleLabel.setStyle(.bold(size: StyleKit.FontSizes.large,
                                  color: StyleKit.Color.primary))

        let descriptionLabel = UILabel()
        descriptionLabel.textAlignment = .center
        descriptionLabel.numberOfLines = 0
        descriptionLabel.setStyle(.regular(size: StyleKit.FontSizes.large,
                                           color: StyleKit.Color.primary))

        let primaryButton = UIButton()
        primaryButton.addTarget(self, action: #selector(didTapPrimaryButton), for: .touchUpInside)
        primaryButton.setStyle(.primary(size: StyleKit.FontSizes.regular))
        primaryButton.snp.makeConstraints {
            $0.height.equalTo(48)
        }

        let infoStack = [imageView, titleLabel, descriptionLabel]
            .stacked(.vertical)
            .alignment(.center)
            .spacing(24)
            .spacing(16, after: titleLabel)

        let mainStack = [infoStack, primaryButton]
            .stacked(.vertical)
            .spacing(32)
        addSubview(mainStack)
        mainStack.snp.makeConstraints {
            $0.leading.trailing.equalToSuperview().inset(24)
            $0.bottom.equalToSuperview().inset(32)
        }

        imageView.snp.makeConstraints {
            $0.centerY.equalTo(self.snp.top)
        }

        self.titleLabel = titleLabel
        self.descriptionLabel = descriptionLabel
        self.primaryButton = primaryButton
    }

    // MARK: Update
    private func updateContent() {
        guard let viewModel = viewModel else {
            defaultContent()
            return
        }

        titleLabel.text = viewModel.title
        descriptionLabel.text = viewModel.description
        primaryButton.setTitle(viewModel.buttonTitle, for: .normal)
    }

    private func defaultContent() {
        titleLabel.text = nil
        descriptionLabel.text = nil
        primaryButton.setTitle(nil, for: .normal)
    }

    // MARK: - Handlers
    @objc
    private func didTapPrimaryButton() {
        delegate?.locationAccessCardViewDidTapButton(self)
    }
}
