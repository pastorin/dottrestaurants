//
//  APIConstants.swift
//  RestaurantsSDK
//
//  Created by Martin Pastorin on 20/12/2019.
//  Copyright © 2019 Martin Pastorin. All rights reserved.
//

import Foundation
import Alamofire

typealias APIHTTPMethod = HTTPMethod
typealias APIParameters = Parameters
typealias APIResult = Result

typealias ParameterEncoding = Alamofire.ParameterEncoding
typealias URLRequestConvertible = Alamofire.URLRequestConvertible
typealias JSONEncoding = Alamofire.JSONEncoding
typealias URLEncoding = Alamofire.URLEncoding


typealias JSON = [String: Any]

enum APIConstants {

    enum ParameterKey {
        static let clientId = "client_id"
        static let clientSecret = "client_secret"
        static let version = "v"
    }

    enum ParameterValue {
        static let versionTimeStamp = "20191220"
    }

    enum Paths {
        static let vanueExplore = "venues/explore"
        static func vanueDetail(_ vanueId: VenueID) -> String {
            return "venues/\(vanueId)"
        }
    }
}

enum HTTPHeaderField {
//    static let contentType = "Content-Type"
//    static let acceptType = "Accept"
}

enum ContentType {
//    static let json = "application/json"
//    static let plainText = "text/plain"
}
