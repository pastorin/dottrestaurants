//
//  JSONEncodable.swift
//  RestaurantsSDK
//
//  Created by Martin Pastorin on 20/12/2019.
//  Copyright © 2019 Martin Pastorin. All rights reserved.
//

import Foundation

protocol JSONEncodable: Encodable {
    typealias Target = Self
}

extension JSONEncodable {

    var encoder: JSONEncoder {
        return JSONEncoder.default
    }

    func encoded() -> JSON {
        do {
            let typed: Target = self
            let jsonData = try encoder.encode(typed)
            let object = try JSONSerialization.jsonObject(with: jsonData, options: [])
            guard let json = object as? JSON else {
                throw SDKError.invalidEncode
            }
            return json
        } catch {
            fatalError("Encoding as JSON failed.")
        }
    }
}
