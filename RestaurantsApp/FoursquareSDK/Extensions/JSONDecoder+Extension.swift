//
//  JSONDecoder+Extension.swift
//  RestaurantsSDK
//
//  Created by Martin Pastorin on 20/12/2019.
//  Copyright © 2019 Martin Pastorin. All rights reserved.
//

import Foundation

extension JSONDecoder {
    static var `default`: JSONDecoder {
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .iso8601
        return decoder
    }
}

extension JSONEncoder {
    static var `default`: JSONEncoder {
        let encoder = JSONEncoder()
        encoder.dateEncodingStrategy = .iso8601
        return encoder
    }
}
