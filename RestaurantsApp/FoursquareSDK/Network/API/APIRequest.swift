//
//  APIRequest.swift
//  RestaurantsSDK
//
//  Created by Martin Pastorin on 20/12/2019.
//  Copyright © 2019 Martin Pastorin. All rights reserved.
//

import Foundation

protocol APIRequest: URLRequestConvertible {
    var method: APIHTTPMethod { get }
    var path: String { get }
    var parameters: APIParameters? { get }
    var encoding: ParameterEncoding { get }
}

extension APIRequest {
    private var config: SDKConfiguration {
        guard let config = FoursquareSDK.shared.configuration else {
            fatalError("SDK not initialized")
        }
        return config
    }

    var baseURL: URL {
        config.serviceURL
    }

    var headers: [String: String] {
        .init()
    }

    var parameters: APIParameters? {
        nil
    }

    var encoding: ParameterEncoding {
        JSONEncoding.default
    }

}

extension APIRequest {
    private var commonParameters: APIParameters {
        return [
            APIConstants.ParameterKey.clientId: config.clientId,
            APIConstants.ParameterKey.clientSecret: config.clientSecret,
            APIConstants.ParameterKey.version: APIConstants.ParameterValue.versionTimeStamp
        ]
    }

    func asURLRequest() throws -> URLRequest {
        let url = try baseURL
            .asURL()
            .appendingPathComponent(path)
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = method.rawValue

        for (key, item) in headers {
            urlRequest.setValue(item, forHTTPHeaderField: key)
        }

        let params = commonParameters.merging(parameters ?? .init()) { $1 }
        return try encoding.encode(urlRequest, with: params)
    }
}
