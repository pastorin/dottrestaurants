//
//  ExploreRequest.swift
//  RestaurantsSDK
//
//  Created by Martin Pastorin on 20/12/2019.
//  Copyright © 2019 Martin Pastorin. All rights reserved.
//

import Foundation

struct ExploreRequest: JSONEncodable {
    let ll: String
    let section: String

    init(coordinate: Coordinate2D, venueType: VenueType) {
        ll = "\(coordinate.latitude),\(coordinate.longitude)"
        section = venueType.rawValue
    }
}
