//
//  VenueAnnotation.swift
//  DottRestaurants
//
//  Created by Martin Pastorin on 21/12/2019.
//  Copyright © 2019 Martin Pastorin. All rights reserved.
//

import Foundation
import MapKit

protocol VenueAnnotationViewModel: MKAnnotation {
    var venueId: SDKVenueID { get }
}

class VenueAnnotation: MKPointAnnotation, VenueAnnotationViewModel {
    // MARK: - Properties
    let venueId: SDKVenueID

    init(_ venue: SDKVenue) {
        venueId = venue.id
        super.init()
        title = venue.name
        coordinate = venue.coordinate
    }
}

extension MKMarkerAnnotationView {
    convenience init(_ annotation: VenueAnnotationViewModel) {
        self.init(annotation: annotation, reuseIdentifier: annotation.venueId)
    }
}
