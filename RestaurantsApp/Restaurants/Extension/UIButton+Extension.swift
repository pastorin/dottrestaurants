//
//  UIButton+Extension.swift
//  DottRestaurants
//
//  Created by Martin Pastorin on 21/12/2019.
//  Copyright © 2019 Martin Pastorin. All rights reserved.
//

import UIKit

// MARK: - Style
extension UIButton {
    enum ButtonStyle {
        case primary(size: CGFloat)
    }

    func setStyle(_ style: ButtonStyle) {
        switch style {
        case .primary(let size):
            backgroundColor = StyleKit.Color.primary
            setTitleColor(StyleKit.Color.white, for: .normal)
            titleLabel?.font = StyleKit.Font.bold(size: size)
            layer.cornerRadius = StyleKit.CornerRadius.regular
        }
    }
}
