//
//  Image.swift
//  FoursquareSDK
//
//  Created by Martin Pastorin on 22/12/2019.
//  Copyright © 2019 Martin Pastorin. All rights reserved.
//

import Foundation

public struct Image: Decodable, Equatable {
    public let url: URL
}

// MARK: - Decodable
public extension Image {
    enum CodingKeys: String, CodingKey {
        case prefix, suffix
    }

    init(from decoder: Decoder) throws {
        let response = try decoder.container(keyedBy: CodingKeys.self)

        let prefix: String = try response.decode(forKey: .prefix)
        let suffix: String = try response.decode(forKey: .suffix)

        guard let imageUrl = URL(string: "\(prefix)original\(suffix)") else {
            throw SDKError.invalidEncode
        }

        url = imageUrl
    }
}
