//
//  Data+Extension.swift
//  RestaurantsSDK
//
//  Created by Martin Pastorin on 20/12/2019.
//  Copyright © 2019 Martin Pastorin. All rights reserved.
//

import Foundation

extension Data {
    static func emptyJSON() -> Data {
        let data = try? JSONSerialization.data(withJSONObject: [:], options: [])
        return data ?? Data()
    }

    func decode<Target: Decodable>(_ decoder: JSONDecoder = .default) throws -> Target {
        let json = isEmpty ? Data.emptyJSON() : self
        return try decoder.decode(Target.self, from: json)
    }
}
