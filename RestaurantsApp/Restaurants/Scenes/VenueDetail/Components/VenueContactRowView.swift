//
//  VenueContactRowView.swift
//  DottRestaurants
//
//  Created by Martin Pastorin on 22/12/2019.
//  Copyright © 2019 Martin Pastorin. All rights reserved.
//

import UIKit

protocol VenueContactRowViewModel {
    var image: UIImage? { get }
    var title: String { get }
}

struct VenueContactRowViewItem: VenueContactRowViewModel {
    let image: UIImage?
    let title: String
}

class VenueContactRowView: UIView {

    // MARK: - Properties
    private(set) weak var imageView: UIImageView!
    private(set) weak var titleLabel: UILabel!

    var viewModel: VenueContactRowViewModel? {
        didSet {
            updateContent()
        }
    }

    // MARK: - Initializer
    convenience init() {
        self.init(frame: .zero)
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupUI()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupUI()
    }

    // MARK: - Setup
    private func setupUI() {
        let imageView = UIImageView()
        imageView.tintColor = StyleKit.Color.primary
        imageView.snp.makeConstraints {
            $0.width.height.equalTo(20)
        }

        let titleLabel = UILabel()
        titleLabel.numberOfLines = 0
        titleLabel.setStyle(.regular(size: StyleKit.FontSizes.regular,
                                     color: StyleKit.Color.label2))


        let mainStack = [imageView, titleLabel]
            .stacked(.horizontal)
            .alignment(.center)
            .spacing(8)
        addSubview(mainStack)
        mainStack.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }

        self.imageView = imageView
        self.titleLabel = titleLabel
    }

    // MARK: Update
    private func updateContent() {
        guard let viewModel = viewModel else {
            defaultContent()
            return
        }

        imageView.image = viewModel.image
        titleLabel.text = viewModel.title
    }

    private func defaultContent() {
        imageView.image = nil
        titleLabel.text = nil
    }

}
