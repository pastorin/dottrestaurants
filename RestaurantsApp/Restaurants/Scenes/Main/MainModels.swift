//
//  MainModels.swift
//  DottRestaurants
//
//  Created by Martin Pastorin on 20/12/2019.
//  Copyright (c) 2019 Martin Pastorin. All rights reserved.
//

import CoreLocation

typealias UserLocation = CLLocation

enum MainModels {
    enum FetchVenues { }
}

extension MainModels.FetchVenues {

    struct Request { }

    struct Response {
        let inProgress: Bool
        let venues: [SDKVenue]
        let viewport: SDKViewport?
    }

    struct ViewModel {
        let inProgress: Bool
        let annotations: [VenueAnnotation]
        let region: Region?
    }
}

