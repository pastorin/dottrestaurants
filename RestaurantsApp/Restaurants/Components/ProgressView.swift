//
//  ProgressView.swift
//  DottRestaurants
//
//  Created by Martin Pastorin on 21/12/2019.
//  Copyright © 2019 Martin Pastorin. All rights reserved.
//

import UIKit
import SwiftMessages

typealias Options = SwiftMessages.Config

class ProgressView: BaseView, Identifiable {

    // MARK: - Singleton
    static var shared = ProgressView()

    // MARK: - Identifiable
    let id = "swiftMessages.progressView"

    // MARK: - Properties
    private(set) weak var messageLabel: UILabel!
    private(set) weak var activityView: UIActivityIndicatorView!

    private let dialog = SwiftMessages()
    private let options: Options = {
        var options = Options()
        options.interactiveHide = false
        options.presentationStyle = .center
        options.dimMode = .gray(interactive: false)
        options.preferredStatusBarStyle = .lightContent
        options.duration = .indefinite(delay: 0.2, minimum: 0)
        return options
    }()

    // MARK: - Initializer
    convenience init() {
        self.init(frame: .zero)
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupUI()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Setup
    func setupUI() {
        let background = CornerRoundingView(frame: .zero)
        background.backgroundColor = StyleKit.Color.background1
        background.cornerRadius = StyleKit.CornerRadius.regular
        background.roundsLeadingCorners = true
        installBackgroundVerticalView(background)
        configureDropShadow()

        backgroundView.backgroundColor = StyleKit.Color.black70

        let content = UIView()
        content.snp.makeConstraints {
            $0.width.height.equalTo(128)
        }

        let activity = UIActivityIndicatorView(style: .large)
        activity.color = StyleKit.Color.white
        activity.hidesWhenStopped = false

        let title = UILabel()
        title.setStyle(.medium(size: StyleKit.FontSizes.regular,
                               color: StyleKit.Color.white))
        title.numberOfLines = 0
        title.textAlignment = .center

        let mainStack = [activity, title]
            .stacked(.vertical)
            .spacing(8)
            .alignment(.center)
        content.addSubview(mainStack)
        mainStack.snp.makeConstraints {
            $0.center.equalToSuperview()
        }

        installContentView(content, insets: .init(top: 8, left: 8, bottom: 8, right: 8))
        messageLabel = title
        activityView = activity
    }

    // MARK: - Presentation
    func present(title: String = "Loading...") {
        messageLabel.text = title
        activityView.startAnimating()
        dialog.show(config: options,
                    view: self)
    }

    func dismiss() {
        dialog.hide(id: id)
    }
}
