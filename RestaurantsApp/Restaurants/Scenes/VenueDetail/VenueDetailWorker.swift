//
//  VenueDetailWorker.swift
//  DottRestaurants
//
//  Created by Martin Pastorin on 22/12/2019.
//  Copyright (c) 2019 Martin Pastorin. All rights reserved.
//

import Foundation
import ReactiveSwift

protocol VenueDetailWorkerInterface: AnyObject {
    typealias RequestVenueDetailResult = Result<SDKVenue, Error>

    var delegate: VenueDetailWorkerDelegate? { get set }
    func fetchVenueDetail(for id: SDKVenueID)
}

protocol VenueDetailWorkerDelegate: AnyObject {
    func workerDidFinishFetchingVenueDetail(_ worker: VenueDetailWorker, with result: VenueDetailWorkerInterface.RequestVenueDetailResult)
}

class VenueDetailWorker: SDKReactiveTarget, VenueDetailWorkerInterface {

    // MARK: - Properties
    weak var delegate: VenueDetailWorkerDelegate?

    // MARK: - VenueDetailWorkerInterface
    func fetchVenueDetail(for id: SDKVenueID) {
        let producer = SDKModule.shared
            .venues
            .details(for: id)
            .materializeResults()
        reactive.venueDetailRequestResult <~ producer
    }
}

// MARK: - Reactive
private extension Reactive where Base: VenueDetailWorker {
    var venueDetailRequestResult: BindingTarget<Base.RequestVenueDetailResult> {
        return makeBindingTarget { worker, result in
            worker.delegate?.workerDidFinishFetchingVenueDetail(worker, with: result)
        }
    }
}
