//
//  VenueDetailViewController.swift
//  DottRestaurants
//
//  Created by Martin Pastorin on 22/12/2019.
//  Copyright (c) 2019 Martin Pastorin. All rights reserved.
//

import UIKit

protocol VenueDetailDisplayInterface: AnyObject {
    func displayVenue(viewModel: VenueDetailModels.FetchVenue.ViewModel)
}

class VenueDetailViewController: UIViewController {

    // MARK: - Properties
    private(set) weak var imageView: UIImageView!
    private(set) weak var infoView: VenueInfoView!
    private(set) weak var contactView: VenueContactView!

    var interactor: VenueDetailInteractorInterface?
    var router: (VenueDetailRouterInterface & VenueDetailDataPassing)?

    // MARK: - Init
    init(data: VenueDetailData) {
        super.init(nibName: nil, bundle: nil)
        let viewController = self
        let presenter = VenueDetailPresenter(displayer: viewController)
        let interactor = VenueDetailInteractor(data: data, presenter: presenter)
        let router = VenueDetailRouter(viewController: viewController, dataStore: interactor)
        viewController.interactor = interactor
        viewController.router = router
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        performFetchVenueRequest()
    }

    // MARK: - Setup
    private func setupUI() {
        view.backgroundColor = StyleKit.Color.white

        let scrollView = UIScrollView()
        scrollView.showsVerticalScrollIndicator = false
        scrollView.contentInsetAdjustmentBehavior = .never
        scrollView.contentInset = .init(top: 0, left: 0, bottom: 40, right: 0)
        view.addSubview(scrollView)
        scrollView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }

        let imageView = UIImageView()
        imageView.backgroundColor = .gray
        imageView.snp.makeConstraints {
            $0.height.equalTo(imageView.snp.width)
        }

        let infoView = VenueInfoView()
        let contactView = VenueContactView()

        let infoStack = [infoView, contactView]
            .stacked(.vertical)
            .spacing(24)
            .layoutMargins(left: 16, right: 16)

        let mainStack = [imageView, infoStack, .spacing]
            .stacked(.vertical)
            .spacing(16)
        scrollView.addSubview(mainStack)
        mainStack.snp.makeConstraints {
            $0.width.equalToSuperview()
            $0.edges.equalToSuperview()
        }

        self.imageView = imageView
        self.infoView = infoView
        self.contactView = contactView
    }

    // MARK: - Helpers
    private func performFetchVenueRequest() {
        interactor?.fetchVenue(request: .init())
    }

    // MARK: - Events
    @objc
    func didTapDismissButton() {
        dismiss(animated: true, completion: nil)
    }
}

// MARK: - Navigation
extension VenueDetailViewController {
    override var navigationItem: UINavigationItem {
        let item = super.navigationItem
        if item.leftBarButtonItem == nil {
            item.leftBarButtonItem = .init(image: UIImage(named: "arrow_down"), style: .plain, target: self, action: #selector(didTapDismissButton))
        }
        return item
    }
}


// MARK: - VenueDetailDisplayInterface
extension VenueDetailViewController: VenueDetailDisplayInterface {

    func displayVenue(viewModel: VenueDetailModels.FetchVenue.ViewModel) {
        imageView.setResource(viewModel.imageUrl)
        infoView.viewModel = viewModel
        contactView.viewModel = viewModel
    }
}
