//
//  VenueGroupResponse.swift
//  FoursquareSDK
//
//  Created by Martin Pastorin on 20/12/2019.
//  Copyright © 2019 Martin Pastorin. All rights reserved.
//

import Foundation

struct VenueGroupResponse: Decodable {
    let venues: [Venue]

    fileprivate struct ItemResponse: Decodable {
        let venue: Venue
    }
}

// MARK: - Decodable
extension VenueGroupResponse {
    enum CodingKeys: String, CodingKey {
        case items
    }

    init(from decoder: Decoder) throws {
        let response = try decoder.container(keyedBy: CodingKeys.self)
        let items: [ItemResponse] = try response.decode(forKey: .items)

        venues = items.reduce(into: []) { result, item in
            result.append(item.venue)
        }
    }
}
