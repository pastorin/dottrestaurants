//
//  SDKError.swift
//  RestaurantsSDK
//
//  Created by Martin Pastorin on 20/12/2019.
//  Copyright © 2019 Martin Pastorin. All rights reserved.
//

import Foundation

public enum SDKError: Error, Equatable {
    case unknown
    case invalidEncode
}
