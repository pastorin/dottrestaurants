//
//  Configuration.swift
//  RestaurantsSDK
//
//  Created by Martin Pastorin on 20/12/2019.
//  Copyright © 2019 Martin Pastorin. All rights reserved.
//

import Foundation

public protocol SDKConfiguration {
    var serviceURL: URL { get }
    var clientId: String { get }
    var clientSecret: String { get }
}
